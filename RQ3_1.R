rm(list = ls())
getwd()

#install and load packages
packages <- c("rgdal", "raster", "spcosa", "clhs", "spsann", "quantregForest")
install.packages(packages)
lapply(packages, require, character.only = TRUE)

#load LUCAS data
load('points/Lucas_proj.Rdata')
cols <- c(1,10, 12, 21,22)
data <- SPROPS.LUCAS_LC[complete.cases(as.data.frame(SPROPS.LUCAS_LC[,cols])),cols]
#load covariate rasters
raster.list <- list.files('stacked1km_rightextend/',pattern =".tif$", full.names=F)
print(raster.list)
pas <- paste('stacked1km_rightextend/',raster.list, sep="")
raster.all <- stack(pas)

#extract raster data to points
for (i in 1: length(raster.list)){
  name <- names(raster.all[[i]])
  value <- extract(raster.all[[i]], data, weights=FALSE, fun=mean, na.omit=TRUE)
  data$col <- value
  names(data)[names(data) == "col"] <- name
  print(paste(i, "of", length(raster.list)))
}
rm(i, value, name, raster.list, pas)

drops <- c('L07USG5_1km_mask' ,"MNGUSG_1km_mask", "C01MCF5_1km_mask","C02MCF5_1km_mask", "C03MCF5_1km_mask", "C04MCF5_1km_mask", "C05MCF5_1km_mask", "C06MCF5_1km_mask", "C07MCF5_1km_mask","C08MCF5_1km_mask","C09MCF5_1km_mask","C10MCF5_1km_mask", "C11MCF5_1km_mask","C12MCF5_1km_mask", "L06USG5_1km_mask")
data <- data[ , !(names(data) %in% drops)]
data <- data[complete.cases(as.data.frame(data)),]

#Create separate spdf with only covariate data
cov <- data[,6:188]
plot(cov, pch=20)

#random sample
rsample <- data[sample(nrow(data), 1000), ] #create random sample
plot(rsample, add=TRUE, pch=15, col='red') #plot sample
ind2 <- c(rsample$POINT_ID) #create vector of POINT_IDs of random sample
index_random <- c() #create empty vector
for (i in 1:length(ind2)) {
  a = which(data$POINT_ID==ind2[i], arr.ind=TRUE) #find Rownumbers of randok samples
  index_random[i] = a
  print(paste(i, "of", length(ind2)))
}
rm(a,i, ind2)
writeOGR(obj=rsample, dsn="sampling_designs", layer="random", driver="ESRI Shapefile", check_exists=TRUE, overwrite_layer = TRUE) #write to shapefile

#clhs
csample <- clhs(cov, size=1000, simple=FALSE)
index_clhs <- csample$index_samples
csample <- data[index_clhs,]
plot(csample,add=TRUE, pch=17, col='green')
writeOGR(obj=csample, dsn="sampling_designs", layer="clhs", driver="ESRI Shapefile", check_exists=TRUE, overwrite_layer = TRUE) # this is in geographical projection
rm(drops)

#spatial coverage
datacol <- c("X_LAEA", "Y_LAEA")
data_spat <- data[datacol]
names(data_spat) <- c("x", "y")
candi <- as.data.frame(data_spat[1:2])[3:4]
names(candi) <- c("x", "y")
spatial <- optimMSSD(1000, candi)
sframe <- spatial$points
spdframe <- SpatialPointsDataFrame(coords = sframe[2:3], proj4string=crs(data), data=sframe[1])
plot(spdframe, add=TRUE, pch=18, col="blue")
index_spatial <- spatial$points$id
ssample <- data[index_spatial,]
writeOGR(obj=ssample, dsn="sampling_designs", layer="spatial_coverage", driver="ESRI Shapefile", check_exists=TRUE, overwrite_layer = TRUE) # this is in geographical projection
#rm(datacol, candi, spatial , sframe)

#Validation data
val <- data[-c(index_clhs,index_random, index_spatial),]
xtest <- na.omit(as.data.frame(val[6:188]))
ytest <- val$pH_in_H2O

#QregForest
nthread = 8
ntree = 1000

#train random
xtrain_random <- as.data.frame(cov[index_random, ])
ytrain_random <- rsample$pH_in_H2O
qrf <- quantregForest(x=xtrain_random, y=ytrain_random, nthread=nthread, ntree=ntree)
conditionalQ <- predict(qrf, xtest)

#calculate range random
minrand <- min(conditionalQ[,3])
maxrand <- max(conditionalQ[,3])
rangerand <- c(minrand, maxrand)
rangerand_diff <- maxrand-minrand

#add predictions to df random
pred <- val[1:3]
pred$pred_pH_rand <- conditionalQ[,3]
cuts <- c(4,5,6,7,8,9,10)

#calculate MSE random
pred$diff_rand <- pred$pH_in_H2O-pred$pred_pH_rand
pred$sqdiff_rand <- pred$diff_rand^2
MSE_rand <- (1/length(pred$sqdiff_rand))*sum(pred$sqdiff_rand)
RMSE_rand <- sqrt(MSE_rand)
#plot random
spplot(pred[2], main='original values', cuts=cuts)
spplot(pred[4], main=paste('predicted', "random"), cuts=cuts)

#train clhs
xtrain_clhs <- as.data.frame(cov[index_clhs,])
ytrain_clhs <- csample$pH_in_H2O
qrf_clhs <- quantregForest(x=xtrain_clhs, y=ytrain_clhs, nthread=nthread, ntree=ntree)
conditionalQ_clhs <- predict(qrf_clhs, xtest)

#calculate range clhs
minclhs <- min(conditionalQ_clhs[3,])
maxclhs <- max(conditionalQ_clhs[3,])
rangeclhs <- c(minclhs, maxclhs)
rangeclhs_diff <- maxclhs-minclhs

#add predictions to df clhs
pred$pred_pH_clhs <- conditionalQ_clhs[,3]

#calculate MSE clhs
pred$diff_clhs <- pred$pH_in_H2O-pred$pred_pH_clhs
pred$sqdiff_clhs <- pred$diff_clhs^2
MSE_clhs <- (1/length(pred$sqdiff_clhs))*sum(pred$sqdiff_clhs)
RMSE_clhs <- sqrt(MSE_clhs)

#plot clhs
spplot(pred[7], main=paste('predicted', 'clhs'), cuts=cuts)

#train spatial
xtrain_spatial <- as.data.frame(cov[index_spatial,])
ytrain_spatial <- ssample$pH_in_H2O
qrf_spatial <- quantregForest(x=xtrain_spatial, y=ytrain_spatial, nthread=nthread, ntree=ntree)
conditionalQ_spatial <- predict(qrf_spatial, xtest)

#calculate range spatial
minspatial <- min(conditionalQ_spatial[3,])
maxspatial <- max(conditionalQ_spatial[3,])
rangespatial <- c(minspatial, maxspatial)
rangespatial_diff <- maxspatial-minspatial

#add spatial prediction to df
pred$pred_pH_spatial <- conditionalQ_spatial[,3]

#calculate MSE spatial
pred$diff_spatial <- pred$pH_in_H2O-pred$pred_pH_spatial
pred$sqdiff_spatial <- pred$diff_spatial^2
MSE_spatial <- (1/length(pred$sqdiff_spatial))*sum(pred$sqdiff_spatial)
RMSE_spatial <- sqrt(MSE_spatial)

#plot spatial
spplot(pred[10], main='predicted spatial', cuts=cuts)
